module go-crawler-distributed

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/garyburd/redigo v1.6.2
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.0 // indirect
	github.com/jinzhu/gorm v1.9.15
	github.com/micro/go-micro/v2 v2.9.1
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	go.uber.org/zap v1.13.0
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2
	golang.org/x/text v0.3.2
	google.golang.org/protobuf v1.23.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
